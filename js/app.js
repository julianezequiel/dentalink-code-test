(function($){

    var step_height= 50; // Px
                
	var Schedule = {
		variables: {
			step: 30, //minutes
			start: '2016-05-16 00:00:00'
		},
		initialize: function() {
			// Render week headers
			for (var i = 0;i<7;i++) {
				var day = moment(this.variables.start).add(i,'days');
				$(".week-header .column-day:eq("+i+")").html(day.format("ddd DD"));
                $(".schedule-container .column-day:eq("+i+")").attr("id", day.format("YYYY-MM-DD")).attr('id');
			}
			// Render schedule
			for (var i = 0;i<24*60/this.variables.step;i++) {
				var time = moment(this.variables.start).add(i*this.variables.step,'minutes');
				var step_element = $("<div class='step' id='"+time.format("HHmm")+"'>"+time.format("HH:mm")+"</div>");
				step_element.css({height: step_height+"px"});
				$(".steps").append(step_element);
			}
			$(".schedule-container").css({
				height: $(".steps").height()+"px"
			});
            
            for(var i = 0; i < $(".schedule-container .column-day").length; i++) {
				var columnDay = $(".schedule-container .column-day:eq("+i+")");
                for (var j = 0;j<24*60/this.variables.step;j++) {
                    var time = moment(this.variables.start).add(j*this.variables.step,'minutes');
                    var step_element = $("<ul class='step-event' id='"+time.format("YYYYMMDDHHmm")+"'></ul>");
                    step_element.css({height: step_height+"px"});
                    columnDay.append(step_element);
                }
            }

			// Fetch data

			$.getJSON("js/data.json",function(r) {
                r["data"].sort(function(event1, event2) {
                    var event1Duration = new Date(event1["due_at"]) - new Date(event1["start_at"]);
                    var event2Duration = new Date(event2["due_at"]) - new Date(event2["start_at"]);
                    return event2Duration - event1Duration;
                });
                if(r["data"] != null && r["data"].length > 0) {
                    $.each(r["data"], function(index, value) {
                        var startDateId = value["start_at"].replace(/-/g, '').replace(/ /g, '').replace(/:/g, '');
                        startDateId = startDateId.substring(0, startDateId.length-2);
                        var endDateId = value["due_at"].replace(/-/g, '').replace(/ /g, '').replace(/:/g, '');
                        endDateId = endDateId.substring(0, endDateId.length-2);
                        var eventDiv = "<li class='not-empty'></li>";
                        $("#"+startDateId).append(eventDiv);
                        
                        var childIndex = $("#"+startDateId).get(0).childElementCount;
                        
				        var nextStartDate = moment(value["start_at"]);
				        var endDate = moment(value["due_at"]);
                        while(true) {
                            nextStartDate = getNextDate(nextStartDate, endDate);
                            if(nextStartDate != undefined) {
                                var nextStartDateId = nextStartDate.format("YYYYMMDDHHmm");
                                var childsInList = $("#"+nextStartDateId).get(0).childElementCount + 1;
                                while(childsInList < childIndex) {
                                    $("#"+nextStartDateId).append("<li class='empty'></li>");
                                    childsInList ++;
                                }
                                $("#"+nextStartDateId).append(eventDiv);
                            } else {
                                break;
                            }
                        }
                        
                    });
                }
                $("#")
			}).error(function(err){
				console.log(err);
			});
		}
	};

	Schedule.initialize();

})(jQuery);


function getNextDate(currentDate, endDate) {
    if(currentDate.valueOf() == endDate.valueOf()) {
        return undefined;   
    } else {
        return moment(currentDate).add(30,'minutes');
    }
}